mod controllers;
mod models;
mod persistence;

use axum::{
    http::Method,
    extract::{Extension},
    routing::get,
    Router,
};
use std::net::SocketAddr;
use tower_http::cors::{Any, CorsLayer};

#[tokio::main]
async fn main() {
    // db-setup
    let pool = persistence::db_setup().await;
    // routes
    let app = Router::new()
        .route("/measurements/:site_id/:rows", get(controllers::get_weather_single_location),)
        .route("/closest_locations/:latitude/:longitude/:count", get(controllers::get_closest_locations),)
        .layer(CorsLayer::new()
            .allow_origin(Any)
            .allow_methods(vec![Method::GET]))
        .layer(Extension(pool));

    // run with hyper
    let addr = SocketAddr::from(([0, 0, 0, 0], 8090));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
