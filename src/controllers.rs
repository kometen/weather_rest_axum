use bb8::{Pool};
use bb8_postgres::PostgresConnectionManager;
use tokio_postgres::{NoTls, Row};
use axum::{extract::{Path, Extension}, http::StatusCode, Json};
use axum::response::{IntoResponse};
use dotenv::Error;
use chrono::{TimeZone, Utc};
use rust_decimal::Decimal;
use rust_decimal::prelude::*;

type ConnectionPool = Pool<PostgresConnectionManager<NoTls>>;

pub(crate) async fn get_weather_single_location(
    Path((site_id, rows)): Path<(i32, i32)>,
    Extension(pool): Extension<ConnectionPool>,
) -> Result<impl IntoResponse, StatusCode> {
    let conn = pool.get().await.map_err(internal_error).unwrap();

    let id = if site_id < 0 { 0 } else { site_id };
    let r = if rows > 144 { 144 } else if rows < 0 { 0 } else { rows };

    println!("get rows");
    let rows= conn
        .query("select * from measurements_single_location_function($1,$2)", &[&id, &r])
        .await
        .map_err(internal_error).unwrap();

    let measurements: Vec<crate::models::MeasurementsSingleLocation> = rows
        .into_iter()
        .map(|row| crate::models::MeasurementsSingleLocation::try_from(&row).unwrap())
        .collect();

    Ok(Json(measurements))
}

pub(crate) async fn get_closest_locations(
    Path((latitude, longitude, count)): Path<(String, String, String)>,
    Extension(pool): Extension<ConnectionPool>,
) -> Result<impl IntoResponse, StatusCode> {
    let conn = pool.get().await.map_err(internal_error).unwrap();

    let lat = match Decimal::from_str(latitude.as_str()) {
        Ok(l) => l,
        Err(_) => Decimal::from_str("0.0").unwrap()
    };

    let lon = match Decimal::from_str(longitude.as_str()) {
        Ok(l) => l,
        Err(_) => Decimal::from_str("0.0").unwrap()
    };

    let c = match count.parse::<i32>() {
        Ok(c) => c,
        Err(_) => 5
    };

    println!("get closest locations");
    let rows = conn
        .query("select * from measurements_closest_locations_function($1,$2,$3)", &[&lat, &lon, &c])
        .await
        .map_err(internal_error).unwrap();

    let closest_locations: Vec<crate::models::ClosestLocations> = rows
        .into_iter()
        .map(|row| crate::models::ClosestLocations::try_from(&row).unwrap())
        .collect();

    Ok(Json(closest_locations))
}

fn internal_error<E>(err: E) -> (StatusCode, String)
    where
        E: std::error::Error,
{
    (StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
}

impl<'a> TryFrom<&'a Row> for crate::models::MeasurementsSingleLocation {
    type Error = Error;

    fn try_from(row: &'a Row) -> Result<Self, Self::Error> {
        let tg_id = row.try_get("id");
        let id = match tg_id {
            Ok(id) => id,
            Err(_) => -1
        };

        let tg_name = row.try_get("name");
        let name = match tg_name {
            Ok(tg) => tg,
            Err(_) => "unknown site".to_string()
        };

        let tg_latitude = row.try_get("latitude");
        let latitude = match tg_latitude {
            Ok(tg) => tg,
            Err(_) => "-0.1".to_string()
        };

        let tg_longitude = row.try_get("longitude");
        let longitude = match tg_longitude {
            Ok(tg) => tg,
            Err(_) => "-0.1".to_string()
        };

        let tg_measurement_time_default = row.try_get("measurement_time_default");
        let measurement_time_default= match tg_measurement_time_default {
            Ok(tg) => tg,
            Err(_) => Utc.ymd(1967, 10, 28).and_hms(10, 11, 12)
        };

        let measurements = row.try_get("measurements").unwrap();
        /*let measurements = match tg_measurements {
            Ok(tg) => tg,
            Err(_) => serde_json::from_str(r#"[{"invalid":true]"#)
        };*/

        Ok(Self {
            id,
            name,
            latitude,
            longitude,
            measurement_time_default,
            measurements
        })
    }
}

impl<'a> TryFrom<&'a Row> for crate::models::ClosestLocations {
    type Error = Error;

    fn try_from(row: &'a Row) -> Result<Self, Self::Error> {
        let tg_id = row.try_get("id");
        let id = match tg_id {
            Ok(id) => id,
            Err(_) => -1
        };

        let tg_name = row.try_get("name");
        let name = match tg_name {
            Ok(tg) => tg,
            Err(_) => "unknown site".to_string()
        };

        let tg_latitude = row.try_get("latitude");
        let latitude = match tg_latitude {
            Ok(tg) => tg,
            Err(_) => "-0.1".to_string()
        };

        let tg_longitude = row.try_get("longitude");
        let longitude = match tg_longitude {
            Ok(tg) => tg,
            Err(_) => "-0.1".to_string()
        };

        let tg_distance = row.try_get("distance");
        let distance = match tg_distance {
            Ok(distance) => distance,
            Err(_) => "-1".to_string()
        };

        let tg_latest_reading = row.try_get("latest_reading");
        let latest_reading= match tg_latest_reading {
            Ok(latest_reading) => latest_reading,
            Err(_) => Utc.ymd(1967, 10, 28).and_hms(10, 11, 12)
        };

        let measurements = row.try_get("measurements").unwrap();
        /*let measurements = match tg_measurements {
            Ok(tg) => tg,
            Err(_) => serde_json::from_str(r#"[{"invalid":true]"#)
        };*/

        Ok(Self {
            id,
            name,
            latitude,
            longitude,
            distance,
            latest_reading,
            measurements
        })
    }
}