Get weather-data from Statens Vegvesen in Norway using REST.

Currently available endpoints:

```
/measurements/:site_id/:rows
```

Where site_id is a locations that measures weather. And rows is number of rows returned, between 0 and 144.

```
/closest_locations/:latitude/:longitude
/closest_locations/:latitude/:longitude/:locations
```
Where latitude/longitude is a position and an optional number of locations. If omitted defaults to 5.

